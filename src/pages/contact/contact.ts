import { Component } from '@angular/core';
import { ModalController,NavController,App,AlertController,LoadingController } from 'ionic-angular';

import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs/Observable";

import { SingletonService } from "../services/service";
import { InvoicesPage } from "../invoices/invoices";

@Component({
  selector: 'page-contact',
  templateUrl: 'contact.html'
})
export class ContactPage {
  ballance : any = '';
  ldata : any = [];
  count : any;
  order:any = 0;
  lim:any = 20;
  items:any = [];
  
  
  constructor(public loadingCtrl: LoadingController,public modalCtrl : ModalController,public app: App,public service : SingletonService,private alertCtrl: AlertController,public navCtrl: NavController,private http: HttpClient) {
    this.langs(JSON.parse(localStorage.getItem("lang")));
  }
  
  ionViewWillEnter(){
    this.getChannelTopup();
    this.getHistory();
  }
  
  doRefresh(refresher) {
    this.getChannelTopup();
    this.getHistory();
    
    setTimeout(() => {
      console.log('Async operation has ended');
      refresher.complete();
    }, 1000);
  }
  
  langs(lang){
    let data:Observable<any>;
    data = this.http.get('assets/data/lang.json');
    data.subscribe(result => {
      for (var i = 0; i < result['lang'].length; ++i) {
        if (result['lang'][i].lang == lang) {
          this.ldata.push(result['lang'][i]);
        }
      }
    })
  }
  
  getChannelTopup(){
    let data = new FormData;
    data.append('id_user', localStorage.getItem('id'));
    this.http.post(this.service.urlRoot+'etopup/getChannelTopup',data).subscribe((result) => {
      this.ballance = result['ballance'];
    });
  }
  
  getHistory(){
    let d:any = [];
   this.items = [];
    this.http.get(this.service.urlRoot+'etopup/getHistory?idu='+localStorage.getItem('id')+'&lim='+this.lim+'&order=0').subscribe((data) => {
      for (var i = 0; i < data['data'].length; ++i) {
        this.items.push({
          'id' : data['data'][i].id,
          'type' : data['data'][i].type,
          'destination' : data['data'][i].destination,
          'info' : data['data'][i].info,
          'msg' : data['data'][i].msg,
          'date' : data['data'][i].date
        });
      }
      this.count = data['data'].length;
    });
  }
  
  doInfinite(infiniteScroll){
    setTimeout(d => {
    this.order = (this.order + this.lim); 
      let data: Observable<any>;
      let valid = this.http.get(this.service.urlRoot+'etopup/getHistory?idu='+localStorage.getItem('id')+'&lim='+this.lim+'&order='+this.order);       
      valid.subscribe(v => {
        if(this.order > v['count']){
          console.log('gak bisa reload lagi bos');
        }else{
          let loading = this.loadingCtrl.create({
            content: "Please Wait.."
          });
          loading.present();  
          
          data = this.http.get(this.service.urlRoot+'etopup/getHistory?idu='+localStorage.getItem('id')+'&lim='+this.lim+'&order='+this.order);
          data.subscribe(result => {
            if(this.order > result['count']){
              console.log('gak bisa reload lagi bos');
              loading.dismiss();
            }else{
              for (var i = 0; i < result['data'].length; ++i) {
                this.items.push({
                  'id' : result['data'][i].id,
                  'destination' : result['data'][i].destination,
                  'info' : result['data'][i].info,
                  'msg' : result['data'][i].msg,
                  'date' : result['data'][i].date
                });
              }
              this.count = result['data'].length;
              loading.dismiss();
            }
          });
        }
      });
      infiniteScroll.complete();
      }, 200);
  }
  
  logout() {
    const confirm = this.alertCtrl.create({
      title: "Logout",
      message: "Would you like to logout?",
      buttons: [
        {
          text: "No",
          handler: () => {
            console.log("Failed");
          }
        },
        {
          text: "Yes",
          handler: () => {
            const root = this.app.getRootNav();
            root.popToRoot();
          }
        }
      ]
    });
    confirm.present();
  }
  
  toModal(id) {
    const modal = this.modalCtrl.create(InvoicesPage,{id: id});
    modal.present();
  }
  
}

