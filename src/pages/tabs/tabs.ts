import { Component } from '@angular/core';

import { AboutPage } from '../about/about';
import { ContactPage } from '../contact/contact';
import { HomePage } from '../home/home';
import { TransferPage } from '../transfer/transfer';
import { AddChildPage } from '../add-child/add-child';

import { NavController, NavParams, App} from 'ionic-angular';
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs/Observable";
import { LoginPage } from "../login/login";
import { ActionSheetController } from 'ionic-angular';
import { ViewChildPage } from '../view-child/view-child';


@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {
  
  log = JSON.parse(localStorage.getItem('login'));
  status:any = 1;
  tab1Root = HomePage;
  tab2Root = AboutPage;
  tab3Root = TransferPage;
  tab4Root = ContactPage;
  tab5Root = AddChildPage;
  
  ldata : any = [];
  ctid:any = [];
  
  constructor(public actionSheetCtrl: ActionSheetController,private http: HttpClient,public navCtrl: NavController, public app : App) {
  //  let  fiveMinutes = 60 * 30;
  //  this.startTimer(fiveMinutes,'05:00'); 
   let d = JSON.parse(localStorage.getItem("lang"));
    if (d == null) {
      const root = this.app.getRootNav();
      root.popToRoot();
    }else{
      this.langs(d);
    }
    this.ctid = localStorage.getItem("ctId");
    if(this.ctid == 3){
      this.status = 0;
    }else{
      this.status = 1;
    }
  }
  
  // startTimer(duration, display) {
  //   let timer = duration;
  //   setInterval(() => {
  //     let minutes,seconds;
      
  //     minutes = parseInt(timer / 60, 10)
  //     seconds = parseInt(timer % 60, 10);
      
  //     minutes = minutes < 10 ? "0" + minutes : minutes;
  //     seconds = seconds < 10 ? "0" + seconds : seconds;
      
  //     display.textContent = minutes + ":" + seconds;
      
  //     if (--timer < 0) {
  //       timer = duration;
  //     }
  //   }, 1000);
  // }
  
  // window.onload = function () {
  // var fiveMinutes = 60 * 30,
  //    display = document.querySelector('#time');
  // startTimer(fiveMinutes, display);
  // };
  
  child() {
    const actionSheet = this.actionSheetCtrl.create({
      title: 'Downline',
      buttons: [
        {
          text: 'View Downline',
          icon : 'list-box',
          handler: () => {
            this.navCtrl.push(ViewChildPage);
          }
        },{
          text: 'Add Downline',
          icon : 'add-circle',
          handler: () => {
            this.navCtrl.push(AddChildPage);
          }
        },{
          text: 'Close',
          role: 'cancel',
          icon : 'close-circle',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    actionSheet.present();
  }
  
  langs(lang){
    let data:Observable<any>;
    data = this.http.get('assets/data/lang.json');
    data.subscribe(result => {
      for (var i = 0; i < result['lang'].length; ++i) {
        if (result['lang'][i].lang == lang) {
          this.ldata.push(result['lang'][i]);
        }
      }
    })
    console.log(this.ldata);
  }
  
}
