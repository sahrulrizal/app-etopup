import { Component } from '@angular/core';
import { HTTP } from '@ionic-native/http';
import { IonicPage, NavController, NavParams,ToastController,AlertController,App } from 'ionic-angular';
import {Validators, FormBuilder, FormGroup } from '@angular/forms';
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs/Observable";
import { HomePage } from '../home/home';
import { TabsPage } from '../tabs/tabs';
import { SingletonService } from "../services/service";
import { NewPasswordPage } from '../new-password/new-password';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  agent_number:any;
  password:any;
  lang:any;
  ldata:any = [];
  private f : FormGroup;

  constructor(
    public app: App,
    private http: HttpClient,
    private alertCtrl: AlertController,
    public service : SingletonService,
    private formBuilder: FormBuilder,
    private toastCtrl: ToastController,
    public navCtrl: NavController,
    public navParams: NavParams
  ) {
  	this.f = this.formBuilder.group({
      agent_number: ['', Validators.required],
      password: ['', Validators.required],
    });

    let d = localStorage.getItem('lang');
    if (d == null) {
      this.langs('1');
      localStorage.setItem("lang", JSON.stringify(1));
    }else{
       this.langs('1');
      localStorage.setItem("lang", JSON.stringify(1));
    }

    console.log(localStorage.getItem('lang'));
  }

  ionViewWillEnter(){
    if (localStorage.getItem('id') != null) {
      this.logout();
    }
  } 
  
  logout() {
    const confirm = this.alertCtrl.create({
      title: "Logout",
      message: "Would you like to logout?",
      buttons: [
        {
          text: "No",
          handler: () => {
              this.navCtrl.push(TabsPage);
          }
        },
        {
          text: "Yes",
          handler: () => {
           localStorage.removeItem('id');
           const root = this.app.getRootNav();
           root.popToRoot();
          }
        }
      ]
    });
    confirm.present();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  toLogin(){
  	let input = this.f.value;
  	let f = this.f;
    let data;
    let body = new FormData();
    body.append('contact_number' , input.agent_number);
    body.append('password' , input.password);
  	data = this.http.post(this.service.urlRoot+'auth/loginEtopup',body);
    data.subscribe(result => {  
      if (result.success == true) {
        let log = Date.now().toString();
        localStorage.setItem("id", result.data.id);
        localStorage.setItem("login",log);
        localStorage.setItem("name", result.data.name);
        localStorage.setItem("number", result.data.contact_number);
        localStorage.setItem("ctId", result.data.ctId);
        localStorage.setItem("ci", result.data.ci);
        localStorage.setItem("email", result.data.email);
        localStorage.setItem("ballance", result.data.ballance);
        localStorage.setItem("idCh", result.data.idch);
        this.toast('Success Login');

        if(result.data.firstLog == "1"){
          this.navCtrl.push(NewPasswordPage);
        }else{
          this.navCtrl.push(TabsPage);
        }

        f.reset();

      }else{
        this.toast("Cannot Login,Please check your Agent Number Or Password!");
      }
    })

  }

  toLang(){
    this.ldata = [];
    localStorage.setItem("lang", JSON.stringify(this.lang));
    let lang = JSON.parse(localStorage.getItem("lang"));
    this.langs(lang);
  }

  langs(lang){
    this.ldata = [];
     let data:Observable<any>;
    data = this.http.get('assets/data/lang.json');
    data.subscribe(result => {
      for (var i = 0; i < result['lang'].length; ++i) {
        if (result['lang'][i].lang == lang) {
          this.ldata.push(result['lang'][i]);
        }
      }
    })
  }

  toast(message) {
  	let toast = this.toastCtrl.create({
  		message: message,
  		duration: 3000,
  		position: 'top'
  	});

  	toast.onDidDismiss(() => {
  		console.log('Dismissed toast');
  	});

  	toast.present();
  }

  forgotPassword() {
    let alert = this.alertCtrl.create({
      title: 'Haluha Password ?',
      inputs: [
        {
          name: 'nomor',
          placeholder: 'Number'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Submit',
          handler: data => {
            let d = new FormData;
            d.append('master_number', data.nomor);
            this.http.post(this.service.urlRoot+'etopup/forgotPassword',d).subscribe(dt => {
              if(dt['success'] == false){
                this.toAlert('Failed', dt['msg']);
              }else if(dt['success'] == true){
                this.toAlert('Success', dt['msg']);
              }
            });
          }
        }
      ]
    });
    alert.present();
  }

  toAlert(title,subtitle) {
    let alert = this.alertCtrl.create({
      title: title,
      subTitle: subtitle,
      buttons: ['Close']
    });
    alert.present();
  }

}
