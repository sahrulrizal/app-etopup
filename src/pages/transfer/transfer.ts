import { Component } from '@angular/core';
import { App, NavController, NavParams,AlertController,LoadingController } from 'ionic-angular';
import {Validators, FormBuilder, FormGroup } from '@angular/forms';
import { HttpClient } from "@angular/common/http";
import { SingletonService } from "../services/service";

/**
* Generated class for the TransferPage page.
*
* See https://ionicframework.com/docs/components/#navigation for more info on
* Ionic pages and navigation.
*/

@Component({
  selector: 'page-transfer',
  templateUrl: 'transfer.html',
})

export class TransferPage {
  ballance:any = '';
  formMt:any ;
  childs:any;
  ch:any;
  to:any;
  ctid:any;
  from:any;
  private f : FormGroup;
  constructor(public app: App,public service : SingletonService,private http: HttpClient,public loadingCtrl: LoadingController,private alertCtrl: AlertController,public navCtrl: NavController, public navParams: NavParams,private formBuilder: FormBuilder ) {
    this.formBuilders();
    this.ctid = localStorage.getItem('ctId');
  }

  ionViewWillEnter(){
    this.getChannelTopup();
  }
  
  formBuilders(){
    this.f = this.formBuilder.group({
      nomor: [''],
      jumlah: ['', Validators.required],
      mt: ['', Validators.required],
      nomorTo: [''],
      deskripsi: [''],
      from :[''],
      from2 :[''],
      from3 :[''],
      fromTo :['']
    });
  }

  mt(){
    this.f.patchValue({nomor:'',nomorTo:''});
    this.formMt = this.f.value.mt;
    if(this.formMt == 2)  {
      this.getChannelID();
    }
    this.getChild();
  }

  selChild(){
    let res = this.f.value.from;
    res = res.split(',');
    this.to = res[0];
    this.f.patchValue({nomor : res[1]});
  }

  selChild2(){
    let res = this.f.value.from2;
    res = res.split(',');
    this.to = res[0];
    this.f.patchValue({nomor : res[1]});
  }

  selChild3(){
    let res = this.f.value.from3;
    res = res.split(',');
    this.to = res[0];
    this.f.patchValue({nomor : res[1]});
  }

  selChildTo(){
    let res = this.f.value.fromTo;
    res = res.split(',');
    this.f.patchValue({nomorTo : res[1]});
  }

  selMt(){
    let m = this.f.value.mt;
    if(m == 1){
      let res = this.f.value.from;
      res = res.split(',');
      this.from = localStorage.getItem('idCh');
      this.to = res[0];
      this.f.patchValue({nomor : res[1]});
    }else if(m == 2){
      this.from = localStorage.getItem('idCh');
      this.f.patchValue({nomor : localStorage.getItem('number')});
      this.to = localStorage.getItem('ci');
    }else if(m == 3){
      let res = this.f.value.from3;
      res = res.split(',');
      this.from = res[0];
      this.f.patchValue({nomor : res[1]});

      let res2 = this.f.value.fromTo;
      res2 = res2.split(',');
      this.to = res2[0];
    }
  }
  
  ionViewDidLoad() {
    this.getChannelTopup();
    console.log('ionViewDidLoad TransferPage');
  }
  
  ballanceTransfer(){
    let alert = this.alertCtrl.create({
      title: 'Confirmation',
      message: 'Are you sure you want to send your balance of $'+this.f.value.jumlah+' USD to '+this.f.value.nomor+'?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Yes',
          handler: () => {
            this.selMt();
            if(this.from == this.to){
              this.toAlert('Failed',"The sender and recipient is can't same!");
            }else if(this.f.value.jumlah < 1){
              this.toAlert('Failed',"The amount must not be less than $1");
            }else if(this.f.value.jumlah >  parseFloat(this.ballance)){
              this.toAlert('Failed',"Can't continue to proccess Transfer Balance, please check your balance!");
            }else{
              const loader = this.loadingCtrl.create({
                content: 'Please Wait...',
              });
              loader.present();

             let body = new FormData;
            body.append('idu',localStorage.getItem('id'));
            body.append('idCh',localStorage.getItem('idCh'));
            body.append('mt',this.formMt);
            body.append('to',this.to);
            body.append('from',this.from);
            body.append('amount',this.f.value.jumlah);
            body.append('deskripsi',this.f.value.deskripsi);
            this.http.post(this.service.urlRoot+'etopup/ballanceTransfer',body,{responseType: 'json'}).subscribe(result => {
              if(result['success'] == true){
                this.toAlert('Success',result['msg']);
                
                let msg = 'Successfully sending balance to '+this.f.value.nomor+' amount $'+this.f.value.jumlah+' USD';
                this.inHistory(this.f.value.nomor,msg,this.f.value.jumlah,"Transfer Balance amount "+this.f.value.jumlah,this.to,this.formMt,this.from);

                this.getChannelTopup();
                this.f.reset();
              }else{
                this.toAlert('Failed',result['msg']);
              }
              loader.dismiss();
            }); 
            }
          }
        }
      ]
    });
    alert.present(); 
  }

  inHistory(dest,msg,amount,transaction,to,mt,from){
    let body = new FormData;
    body.append('idu',localStorage.getItem('id'));
    body.append('destination',dest);
    body.append('amount',amount);
    body.append('transaction',transaction);
    body.append('msg',msg);
    body.append('to',to);
    body.append('mt',mt);
    body.append('from',from);
    
    this.http.post(this.service.urlRoot+'etopup/inHistory',body,{responseType: 'text'}).subscribe(result => {
      console.log(result);
    });  
  }
  
  getChannelTopup(){
    let data = new FormData;
    data.append('id_user', localStorage.getItem('id'));
    this.http.post(this.service.urlRoot+'etopup/getChannelTopup',data).subscribe((result) => {
      this.ballance = result['ballance'];
    });
  }

  getChild(){
    let data = new FormData;
    data.append('id_user', localStorage.getItem('id'));
    this.http.post(this.service.urlRoot+'etopup/getChild?idCh='+localStorage.getItem('idCh'),data).subscribe((result) => {
      this.childs = result;
    });
  }
  
  toAlert(title,subtitle) {
    let alert = this.alertCtrl.create({
      title: title,
      subTitle: subtitle,
      buttons: ['Close']
    });
    alert.present();
  }

  getChannelID(){
    this.http.get(this.service.urlRoot+'etopup/getChannelID?id='+localStorage.getItem('ci')).subscribe(data => {
      this.ch = data['channel_name'];
      this.f.patchValue({nomor : data['master_number']});
    });
  }

  logout() {
    const confirm = this.alertCtrl.create({
      title: "Logout",
      message: "Would you like to logout?",
      buttons: [
        {
          text: "No",
          handler: () => {
            console.log("Failed");
          }
        },
        {
          text: "Yes",
          handler: () => {
           localStorage.removeItem('id');
           const root = this.app.getRootNav();
           root.popToRoot();
          }
        }
      ]
    });
    confirm.present();
  }
}
