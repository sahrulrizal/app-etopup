import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,AlertController,LoadingController } from 'ionic-angular';
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs/Observable";

import { SingletonService } from "../services/service";
import {Validators, FormBuilder, FormGroup } from '@angular/forms';

/**
 * Generated class for the PaketDataPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-paket-data',
  templateUrl: 'paket-data.html',
})
export class PaketDataPage {
	 private todo : FormGroup;
  	items :any = [];
  	lim = 0;
  	perPage = 3;
  	myInput;
    bantu : any;
    offset:any = 0;

    info; /** untuk alert pencarian. */
    token:any;
    urlRoot:any = this.service.urlRoot;

  constructor(public loadingCtrl: LoadingController,private alertCtrl: AlertController,private formBuilder: FormBuilder,public navCtrl: NavController, public navParams: NavParams,private http: HttpClient,public service : SingletonService) {
  	 this.todo = this.formBuilder.group({
      nomor: ['', Validators.required],
    });
  }

  ionViewDidLoad() {
    this.getRefilBonita(this.offset);
  }

  getToken(){
    return new Promise((resolve, reject) => {
    	let url = this.service.url;
    	this.http.get(url+'token')
    	.subscribe(
    		data => {
    			resolve(data)
    		},
    		error => {
    			reject(error);
    		},
    	);
    });    
  }

  rend(min,max){
    return min + Math.floor(Math.random() * (max - min + 1));
  }

  prosesPaketData(id,name,amount,quota,validity){
       this.getToken().then(token => {
    const loader = this.loadingCtrl.create({
      content: 'Please Wait...',
    });
    loader.present();

    let url = this.service.url;
    let url2 = this.service.url2;
    let body = new FormData;
    body.append('token',this.token.access_token);
    body.append('msisdn',this.todo.value.nomor);
    body.append('amount',amount);
    body.append('trxid',this.rend(10,100000000));
    this.http.post(this.urlRoot+'etopup/isiTopup',body,{responseType: 'text'}).subscribe(result2 => {
      let res = [];
      res = result2.split(' ');
      if (res[2] == 0) {
        body.append('refilId',id);
        body.append('name',name);
        this.http.post(this.urlRoot+'etopup/refilPackage',body,{responseType: 'text'}).subscribe(result2 => {console.log(result2)});
        this.http.post(this.urlRoot+'etopup/sendSmsPaketData',body,{responseType: 'text'}).subscribe(result3 => {console.log(result3)});
         this.toAlert('success','Sukses menambahkan paket data '+name+' '+quota+', dengan harga $'+amount+' ke nomor '+this.todo.value.numeru);
        loader.dismiss();
      }else{
        this.toAlert('Failed',"Can't continue to proccess E-topup, please check your number!");
        loader.dismiss();
      }
    });  
    });
  }

  getRefilBonita(offset){
  	let token = <any>{};
 	this.getToken().then(token => {
    this.token = token;
 
    let url = this.service.url;
    let url2 = this.service.url2;
    
    let body: FormData = new FormData();
    body.append('token',this.token.access_token);
    body.append('offset',offset);

    console.log(offset);
    let data = this.http.post(url2+'getRefilBonita',body);
    data.subscribe(result => {
       let arr:any = [];
       arr = result; 
       for (var i = 0; i < arr.length; ++i) {
        this.items.push(result[i]);
      }
    });

    });
  }

  getEtopup(lim,page){
    let data:Observable<any>;
    data = this.http.get('http://150.242.111.235/apibonita/index.php/etopup/getEtopupDenomMobile/'+lim+'/'+page);
    data.subscribe(result => {
    	for (var i = 0; i < result.data.length; ++i) {
	      this.items.push(result.data[i]);
    	}
    })
  }

  getEtopupFind(find=''){
  	 this.getToken();
    let url = this.service.url;
    let url2 = this.service.url2;
    
    let body: FormData = new FormData();
    body.append('token',this.token);
    let data = this.http.post(url2+'getRefilBonita',body);
    data.subscribe(result => {
       this.items = result;
    });  
  }

  onInput(){
    this.items = [];
    this.lim = 0;
    this.perPage = 3;
    if (this.myInput == "") {
     this.getEtopup(this.lim,this.perPage);
     this.info = 1;
    }else{
     this.getEtopupFind(this.myInput); 
  	}
  }

  doInfinite(infiniteScroll) {
    this.offset = this.offset + 5;
    this.getRefilBonita(this.offset);	
    setTimeout(() => {
      console.log('Async operation has ended');
      infiniteScroll.complete();
    }, 1000);
  }

   toAlert(title,subtitle) {
    let alert = this.alertCtrl.create({
      title: title,
      subTitle: subtitle,
      buttons: ['Dismiss']
    });
    alert.present();
  }

  inPaketData(id,name,amount,quota,validity) {
    let alert = this.alertCtrl.create({
      title: 'Konfirmasi Pembelian',
      message: 'Apakah anda yakin ingin menambahkan paket data '+name+' '+quota+', dengan harga $'+amount+' ?',
      buttons: [
      {
        text: 'Cancel',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      },
      {
        text: 'Yes',
        handler: () => {
          this.prosesPaketData(id,name,amount,quota,validity);
          console.log('Buy clicked');
        }
      }
      ]
    });
    alert.present();
  }

}
