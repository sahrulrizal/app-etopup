import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,AlertController,LoadingController } from 'ionic-angular';
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs/Observable";

import { SingletonService } from "../services/service";
import {Validators, FormBuilder, FormGroup } from '@angular/forms';

/**
 * Generated class for the IsiPulsaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-isi-pulsa',
  templateUrl: 'isi-pulsa.html',
})
export class IsiPulsaPage {
    private todo : FormGroup;
  	items = [];
  	lim = 0;
  	perPage = 3;
  	myInput;
    bantu : any;

    info:any; /** untuk alert pencarian. */
    token:any;
    urlRoot:any = this.service.urlRoot;


  constructor(public loadingCtrl: LoadingController,private alertCtrl: AlertController,private formBuilder: FormBuilder,public navCtrl: NavController, public navParams: NavParams,private http: HttpClient,public service : SingletonService) {
  	 this.todo = this.formBuilder.group({
      nomor: ['', Validators.required],
    });
  }

  ionViewDidLoad() {
  	this.getEtopup(this.lim,this.perPage);
    this.getToken();
  }

  getToken(){

    let i:Observable<any>;
    let url = this.service.url;
     i = this.http.get(url+'token');
    i.subscribe((data) => {
      this.token = data.access_token;
    });
  }

  rend(min,max){
    return min + Math.floor(Math.random() * (max - min + 1));
  }

  prosesTopup(ided,denom,amount,validity){
   const loader = this.loadingCtrl.create({
      content: 'Please Wait...',
    });
    loader.present();

    this.getToken();
    let url = this.service.url;
    let url2 = this.service.url2;
    let urlRoot = this.service.urlRoot;
    let addTopup = new FormData;
    
    addTopup.append('msisdn',this.todo.value.nomor);
    addTopup.append('amount',amount);
    addTopup.append('ided',ided);
    addTopup.append('iduser',localStorage.getItem('id'));
    addTopup.append('channel',localStorage.getItem('idCh'));
    
    let data = this.http.post(urlRoot+'/etopup/addTopUpMobile',addTopup,{responseType: 'json'});
    data.subscribe(result => {
      if (result['success'] == true) {
        
        addTopup.append('token',this.token);
        addTopup.append('validity',validity);
        addTopup.append('trxid',this.rend(10,100000000000));

        this.http.post(urlRoot+'etopup/isiTopup',addTopup,{responseType: 'text'}).subscribe(result2 => {
          let res = [];
          res = result2.split(' ');
          if (res[2] == 0) {
              this.http.post(urlRoot+'etopup/sendSmsTopUp',addTopup).subscribe(result3 => {console.log(result3)});
              this.http.get(urlRoot+'etopup/updateStatusTrx?id='+result['etopup_id']+'&idu='+localStorage.getItem('id')+'&amount='+amount+'&msisdn='+this.todo.value.nomor).subscribe(result4 => {console.log(result4); });
              this.toAlert('success','Sukses menambahkan pulsa '+denom+' USD, dengan harga $'+amount);
              loader.dismiss();
          }else{
            this.toAlert('Failed',"Can't continue to proccess E-topup, please check your number!");
              loader.dismiss();
          }
        });  
      }
    });
  }


  getEtopup(lim,page){
    let data:Observable<any>;
    data = this.http.get(this.urlRoot+'etopup/getEtopupDenomMobile/'+lim+'/'+page);
    data.subscribe(result => {
    	for (var i = 0; i < result.data.length; ++i) {
	      this.items.push(result.data[i]);
    	}
    })
  }

  getEtopupFind(lim,page,find=''){
    let data:Observable<any>;
    if (find) {      
      data = this.http.get(this.urlRoot+'etopup/getEtopupDenomMobile/'+lim+'/'+page+'/'+find);
      data.subscribe(result => {
        for (var i = 0; i < result.data.length; ++i) {
          this.items.push(result.data[i]);
        }
        if(result.status == 0){
          this.info = 0;
        }else{
          this.info = 1;
        }

      })
    }
  }

  onInput(){
    this.items = [];
    this.lim = 0;
    this.perPage = 3;
    if (this.myInput == "") {
     this.getEtopup(this.lim,this.perPage);
     this.info = 1;
    }else{
     this.getEtopupFind(this.lim,this.perPage,this.myInput); 
  	}
  }

  doInfinite(infiniteScroll) {

    setTimeout(() => {
    this.lim  = this.lim + this.perPage;
     this.getEtopup(this.lim,this.perPage);	
      console.log('Async operation has ended');
      infiniteScroll.complete();
    }, 500);
  }

  toAlert(title,subtitle) {
    let alert = this.alertCtrl.create({
      title: title,
      subTitle: subtitle,
      buttons: ['Dismiss']
    });
    alert.present();
  }

  inTopup(ided,denom,amount,validity) {
    let alert = this.alertCtrl.create({
      title: 'Konfirmasi Pembelian',
      message: 'Apakah anda yakin ingin menambahkan pulsa '+denom+' USD, dengan harga $'+amount+'?',
      buttons: [
      {
        text: 'Cancel',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      },
      {
        text: 'Yes',
        handler: () => {
          this.prosesTopup(ided,denom,amount,validity);
          console.log('Buy clicked');
        }
      }
      ]
    });
    alert.present();
  }

}
