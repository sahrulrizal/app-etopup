import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { IsiPulsaPage } from './isi-pulsa';

@NgModule({
  declarations: [
    IsiPulsaPage,
  ],
  imports: [
    IonicPageModule.forChild(IsiPulsaPage),
  ],
})
export class IsiPulsaPageModule {}
