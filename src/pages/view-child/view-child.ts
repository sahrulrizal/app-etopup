import { Component } from '@angular/core';
import { IonicPage,App, NavController, NavParams,AlertController} from 'ionic-angular';
import { HttpClient } from "@angular/common/http";

import { SingletonService } from "../services/service";
import { AddChildPage } from '../add-child/add-child';
import { EditChildPage } from '../edit-child/edit-child';

/**
 * Generated class for the ViewChildPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-view-child',
  templateUrl: 'view-child.html',
})
export class ViewChildPage {

  list:any;
  pushAdd:any;
  constructor(private alertCtrl: AlertController,public app: App,public service : SingletonService,private http: HttpClient,public navCtrl: NavController, public navParams: NavParams) {
    this.pushAdd = AddChildPage;
  }

  ionViewWillEnter(){
    this.getChild();
  }

  // GET

  getChild(){
    this.http.get(this.service.urlRoot+'etopup/getViewChild?id='+localStorage.getItem('idCh')).subscribe(data => {
      if(data['success'] == false){
        this.toAlert('Failed', data['msg']);
      }else if(data['success'] == true){
        this.list = data['data'];
      }
    });
  }

    // INFO

    toAlert(title,subtitle) {
      let alert = this.alertCtrl.create({
        title: title,
        subTitle: subtitle,
        buttons: ['Close']
      });
      alert.present();
    }

    // To

    toEditChild(id){
      this.navCtrl.push(EditChildPage,{id : id});
    }

}
