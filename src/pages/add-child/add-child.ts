import { Component } from '@angular/core';
import { App, NavController, NavParams,AlertController,LoadingController } from 'ionic-angular';
import { HttpClient } from "@angular/common/http";
import {Validators, FormBuilder, FormGroup,FormControl } from '@angular/forms';
import { Geolocation } from '@ionic-native/geolocation';

import { SingletonService } from "../services/service";
import { ViewChildPage } from '../view-child/view-child';

/**
* Generated class for the AddChildPage page.
*
* See https://ionicframework.com/docs/components/#navigation for more info on
* Ionic pages and navigation.
*/

@Component({
  selector: 'page-add-child',
  templateUrl: 'add-child.html',
})
export class AddChildPage {
  
  pushView:any;
  district:any;
  subDistrict:any;
  salesForce:any;
  child:any;
  lat:any;
  lng:any;
  
  private f : FormGroup;
  constructor(private geolocation: Geolocation,public app: App,public service : SingletonService,private http: HttpClient,public loadingCtrl: LoadingController,private alertCtrl: AlertController,public navCtrl: NavController, public navParams: NavParams,private formBuilder: FormBuilder) {
    this.formBuilders();
    this.getDistrict();
    this.getSalesForce();
    this.pushView = ViewChildPage;
  }
  
  formBuilders(){
    this.f = this.formBuilder.group({
      child: ['', Validators.required],
      name: ['', Validators.required],
      district: [''],
      sub_district: [''],
      address: [''],
      sales_force : [''],
      lat : [''],
      lng : [''],
      master_number: ['', Validators.required],
      contact_number: [''],
      email : ['']
      // email: new FormControl('', Validators.compose([
      //   Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
      // ]))
    });
  }
  
  // ACTION
  
  selChild(){
    this.child = this.f.value.child;
  }

  sleksiNomor(no){
    let jml = no.length;

    if(jml == 8){
      if((no[0] == '7' && no[1] == '3') || (no[0] == '7' && no[1] == '4')){
        return no;
      }else{
       this.toAlert('Failed', 'Initial number is not allowed, the initial number must be 73 and 74');
      }
    }else{
      this.toAlert('Failed', 'The number you entered must be 8 characters');
    }
  }

  selDistrict(){
    let id = this.f.value.district;
    this.getSubDistrict(id);
  }
  
  toAddChild(){
    let data = new FormData;

    let no = this.f.value.master_number;
    let jml = no.length;
    if(jml == 8){
      if((no[0] == '7' && no[1] == '3') || (no[0] == '7' && no[1] == '4')){
        
        const loader = this.loadingCtrl.create({
          content: 'Please Wait...',
        });
        loader.present();

        data.append("child" , this.f.value.child);
        data.append("name" , this.f.value.name);
        data.append("district" , this.f.value.district);
        data.append("sub_district" , this.f.value.sub_district);
        data.append("address" , this.f.value.address);
        data.append("sales_force" , this.f.value.sales_force),
        data.append("lat" , this.f.value.lat);
        data.append("lng" , this.f.value. lng);
        data.append("contact_number" , this.f.value.contact_number);
        data.append("master_number" , this.f.value.master_number);
        data.append("email" , this.f.value.email);
        // LocalStorage
        data.append('idc', localStorage.getItem('idCh'));
        data.append('idu', localStorage.getItem('id'));
  
        this.http.post(this.service.urlRoot+'etopup/toAddChild',data).subscribe(data => {
          if(data['success'] == false){
              this.toAlert('Failed', data['msg']); 
              if(data['info'] == 1){  
                this.f.patchValue({name : ''});
              }else if(data['info'] == 2){
                this.f.patchValue({master_number : ''});
              }   
          }else if(data['success'] == true ){
            this.toAlert('Success', data['msg']);
            this.f.reset();
          }
          loader.dismiss();
        });
      
      }else{
       this.toAlert('Failed', 'Initial number is not allowed, the initial number must be 73 or 74');
      }
    }else{
      this.toAlert('Failed', 'The number you entered must be 8 characters');
    }
     
    } 

    // INFO

    toAlert(title,subtitle) {
      let alert = this.alertCtrl.create({
        title: title,
        subTitle: subtitle,
        buttons: ['Close']
      });
      alert.present();
    }
    
    // GET

    getLocation(){
      const loader = this.loadingCtrl.create({
        content: 'Please Wait...',
      });
      loader.present();

      this.geolocation.getCurrentPosition().then((resp) => {
        this.f.patchValue({lat : resp.coords.latitude});
        this.f.patchValue({lng : resp.coords.longitude});
        loader.dismiss();
      }).catch((error) => {
        console.log('Error getting location', error);
        loader.dismiss();
       });
    }
    
    getDistrict(){
      this.http.get(this.service.urlRoot+'etopup/getDistrict').subscribe(data => {
        this.district = data['data'];
      });
    }
    
    getSubDistrict(id){
      this.http.get(this.service.urlRoot+'etopup/getSubDistrict?id='+id).subscribe(data => {
        this.subDistrict = data['data'];
      });
    }
    
    getSalesForce(){
      this.http.get(this.service.urlRoot+'etopup/getSalesTeam').subscribe(data => {
        this.salesForce = data['data'];
      });
    }
    
  }
  