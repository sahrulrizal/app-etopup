import { Component } from '@angular/core';
import { HTTP } from '@ionic-native/http';

import { IonicPage,App, NavController, NavParams,ToastController,AlertController } from 'ionic-angular';
import { HttpClient,HttpHeaders,HttpRequest,HttpParams} from "@angular/common/http";
import { Observable } from "rxjs/Observable";
import { SingletonService } from "../services/service";


/**
 * Generated class for the HistoryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-history',
  templateUrl: 'history.html',

})

export class HistoryPage {
dari: String = new Date('2018-07-19').toISOString();
sampai: String = new Date('2018-08-08').toISOString();
  msg : any;
  items:any = [];	
  constructor(private service: SingletonService,public navCtrl: NavController,private http : HttpClient, public navParams: NavParams) {
  }

  ionViewDidLoad() {
  	this.getHistory();
  }

  getHistory(){
  	this.http.get(this.service.urlRoot+'etopup/getHistory?idu='+localStorage.getItem('id')).subscribe((data) => {
  		this.items = data;
  	});
  }

}
