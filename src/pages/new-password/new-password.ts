import { Component } from '@angular/core';
import { NavController, NavParams,AlertController,App} from 'ionic-angular';
import {Validators, FormBuilder, FormGroup } from '@angular/forms';
import { HttpClient } from "@angular/common/http";
import { SingletonService } from "../services/service";
import { TabsPage } from '../tabs/tabs';

/**
 * Generated class for the NewPasswordPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-new-password',
  templateUrl: 'new-password.html',
})
export class NewPasswordPage {

  private f : FormGroup;

  constructor(
    public app: App,
    public navCtrl: NavController, public navParams: NavParams,
    private http: HttpClient,
    private alertCtrl: AlertController,
    public service : SingletonService,
    private formBuilder: FormBuilder
  ) {
    // disini
    this.formBuilders();
  }

  ionViewDidLoad() {

  }

  /* TO */

  toInNewPassword(){

    if(this.f.value.password != this.f.value.newPassword){
      this.toAlert("Sorry your password is not the same","Failed");
    }else{
    let d = new FormData;
    d.append('password', this.f.value.password);
    d.append('id', localStorage.getItem('id'));
    this.http.post(this.service.url2+'inNewPassword',d).subscribe(data => {
      if(data['success'] == true){
        this.toAlert(data['msg'],"Success");
        this.navCtrl.push(TabsPage);
      }else{
        this.toAlert(data['msg'],"Failed");
      }
    });
   }
  }

  // PUBLIC

  formBuilders(){
    this.f = this.formBuilder.group({
      password: ['', Validators.required],
      newPassword: ['', Validators.required],
    });
  }

  toAlert(title,subtitle) {
    let alert = this.alertCtrl.create({
      title: title,
      subTitle: subtitle,
      buttons: ['Close']
    });
    alert.present();
  }

  ionViewWillLeave(){
    
  }

  logout() {
    const confirm = this.alertCtrl.create({
      title: "Logout",
      message: "Would you like to logout?",
      buttons: [
        {
          text: "No",
          handler: () => {
            console.log("Failed");
          }
        },
        {
          text: "Yes",
          handler: () => {
           localStorage.removeItem('id');
           const root = this.app.getRootNav();
           root.popToRoot();
          }
        }
      ]
    });
    confirm.present();
  }
}
