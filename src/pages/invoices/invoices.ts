import { Component } from '@angular/core';
import { NavController, NavParams,LoadingController,AlertController } from 'ionic-angular';
import { SingletonService } from "../services/service";
import { HttpClient } from "@angular/common/http";

/**
 * Generated class for the InvoicesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-invoices',
  templateUrl: 'invoices.html',
})
export class InvoicesPage {
  history :any = [];
  email :any = [];
  femail:any;
  fnamapenerima:any;
  fname:any;
  constructor(public navCtrl: NavController,private alertCtrl: AlertController,public loadingCtrl: LoadingController, public navParams: NavParams,public service : SingletonService,private http: HttpClient) {
  }

  ionViewDidLoad() {
    this.getHistoryID();
    console.log('ionViewDidLoad InvoicesPage', localStorage.getItem('email'));
    
  }

  getHistoryID(){
    let d = new FormData;
    d.append('id',  this.navParams.get('id'));
    this.http.post(this.service.urlRoot+'etopup/getHistoryID',d).subscribe((data) => {
      this.history = data['history'];
      this.fnamapenerima = this.history.name;
      this.femail = data['email'];
    });
  }

  sendInvoice(id,email){
    const loader = this.loadingCtrl.create({
      content: 'Please Wait...',
    });
    loader.present();

    let d = new FormData;
    d.append('id',  id);
    d.append('email', this.femail);
    d.append('nama_penerima', this.fnamapenerima);
    d.append('uemail', localStorage.getItem('email'));
    this.http.post(this.service.urlRoot+'etopup/sendInvoice',d).subscribe((data) => {
      console.log(data);
      let msg = 'Sukses mengirim invoice';
      this.toAlert('Success',msg);
      loader.dismiss();
      this.navCtrl.pop();
    });
  }

  toAlert(title,subtitle) {
    let alert = this.alertCtrl.create({
      title: title,
      subTitle: subtitle,
      buttons: ['Close']
    });
    alert.present();
  }

  close() {
    this.navCtrl.pop();
  }

}
