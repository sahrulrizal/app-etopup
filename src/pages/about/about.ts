import { Component } from '@angular/core';
import { IonicPage,App, NavController, NavParams,AlertController,LoadingController } from 'ionic-angular';
import { NativeGeocoder, NativeGeocoderReverseResult,NativeGeocoderOptions } from '@ionic-native/native-geocoder';
import { Diagnostic } from "@ionic-native/diagnostic";
import { OpenNativeSettings } from "@ionic-native/open-native-settings";
import { HttpClient } from "@angular/common/http";
import { Geolocation } from '@ionic-native/geolocation';
import { Observable } from "rxjs/Observable";
import {Validators, FormBuilder, FormGroup } from '@angular/forms';

import { SingletonService } from "../services/service";


@Component({
  selector: 'page-about',
  templateUrl: 'about.html'
})
export class AboutPage {
  private f : FormGroup;
  ldata : any = [];
  rtb:any = [];
  items :any = [];
  lim = 0;
  perPage = 3;
  myInput;
  bantu : any;
  offset:any = 0;
  balance:any;
  location : any;
  
  info; /** untuk alert pencarian. */
  token:any;
  urlRoot:any = this.service.urlRoot;
  
  constructor(private geolocation: Geolocation, private setting : OpenNativeSettings,private diagnostic: Diagnostic,private nativeGeocoder: NativeGeocoder,public app: App,public loadingCtrl: LoadingController,public service : SingletonService,private alertCtrl: AlertController,public navCtrl: NavController,private formBuilder: FormBuilder,private http: HttpClient) {
    this.formBuilders();
    this.langs(JSON.parse(localStorage.getItem("lang")));
    this.getRefilBonita(this.offset="");
    // this.doInfinite();
    let successCallback = isAvailable => {
      if (isAvailable == true) {
      } else {

      }
    };
  }

  getLocation(){
    let options: NativeGeocoderOptions = {
      useLocale: true,
      maxResults: 5
    };

    this.geolocation.getCurrentPosition().then((resp) => {
      this.nativeGeocoder.reverseGeocode(-8.5537992,125.5393888, options)
      .then((result: NativeGeocoderReverseResult[]) => {
        let info =  JSON.stringify(result[0].administrativeArea);
        this.location = info;
        // this.toAlert('Berhasil',info);
      })
      .catch((error: any) => console.log(error));
      
    }).catch((error) => {
         this.setting.open("location");
    });
  }

  
  langs(lang){
    let data:Observable<any>;
    data = this.http.get('assets/data/lang.json');
    data.subscribe(result => {
      for (var i = 0; i < result['lang'].length; ++i) {
        if (result['lang'][i].lang == lang) {
          this.ldata.push(result['lang'][i]);
        }
      }
    })
  }
  
  
  formBuilders(){
    this.f = this.formBuilder.group({
      numeru: ['', Validators.required],
      data_package: ['', Validators.required],
    });
  }
  
  ionViewWillEnter(){
    this.getChannelTopup();
    this.getRefilBonita(this.offset="");
    console.log(this.balance);
  }
  
  getToken(){
    return new Promise((resolve, reject) => {
      let url = this.service.url;
      this.http.get(url+'token')
      .subscribe(
        data => {
          resolve(data)
        },
        error => {
          reject(error);
        },
      );
    });    
  }
  
  rend(min,max){
    return min + Math.floor(Math.random() * (max - min + 1));
  }
  
  prosesPaketData(id,name,amount,quota,validity){

    this.getToken().then(token => {
      const loader = this.loadingCtrl.create({
        content: 'Please Wait...',
      });
      loader.present();
      
      if(JSON.parse(amount) > JSON.parse(this.balance)){
        this.toAlert('Failed',"Can't continue to proccess E-topup, please check your balance!");
        loader.dismiss();
      }else{
        
        let url = this.service.url;
        let url2 = this.service.url2;
        let body = new FormData;
        
        if(this.location == undefined){
          this.location = JSON.stringify('');
        }

        let lang;
        if (JSON.parse(localStorage.getItem("lang")) == 1) {
           lang = 'TL';
        }else if(JSON.parse(localStorage.getItem("lang")) == 2){
          lang = 'EN';
        }else if(JSON.parse(localStorage.getItem("lang")) == 3){
          lang = 'ID';
        }

        body.append('lang',lang);
        body.append('msisdn',this.f.value.numeru);
        body.append('amount',amount);
        body.append('n_package',name);
        body.append('ided',id);
        body.append('location',JSON.parse(this.location));
        body.append('info', '2');
        body.append('isMobile', '1');
        body.append('iduser',localStorage.getItem('id'));
        body.append('channel',localStorage.getItem('idCh'));
        
        let data = this.http.post(this.urlRoot+'/etopup/isiPaketDataMobile',body,{responseType: 'json'});
        data.subscribe(d => {
          if (d['success'] == true) {
            
            body.append('token',this.token.access_token);
            body.append('trxid',this.rend(10,100000000));
            this.http.post(this.urlRoot+'etopup/isiTopup',body,{responseType: 'text'}).subscribe(result2 => {
              let res = [];
              res = result2.split(' ');
              if (res[2] == 0) {
                
                body.append('refilId',id);
                body.append('name',name);
                body.append('transaction',"Paket Data "+name);
                
                this.http.post(this.urlRoot+'etopup/refilPackage',body,{responseType: 'text'}).subscribe(result2 => {console.log(result2)});
                this.http.get(this.urlRoot+'etopup/updateStatusTrx?id='+d['etopup_id']+'&idu='+localStorage.getItem('id')+'&amount='+amount+'&msisdn='+this.f.value.numeru+'&via=MOBILE_PULSA_DIGITAL').subscribe(result4 => {console.log(result4); });
                this.http.post(this.urlRoot+'etopup/sendSmsPaketData',body,{responseType: 'text'}).subscribe(result3 => {console.log(result3)});
                let msg = 'Success adding data package '+name+' '+quota+', with price $'+amount+' to number '+this.f.value.numeru;
                // this.inHistory(this.f.value.numeru,msg,amount,"Paket Data "+name,'2');
                this.toAlert('Success',msg);
                loader.dismiss();
                this.f.reset();
              }else{
                this.toAlert('Failed',"Can't continue to proccess E-topup, please check your number!");
                loader.dismiss();
              }
            });  
            
          }
        });
      }
      });
  }
  
  inHistory(dest,msg,amount,transaction,info){
    let body = new FormData;
    body.append('idu',localStorage.getItem('id'));
    body.append('destination',dest);
    body.append('msg',msg);
    body.append('info',info);
    body.append('amount',amount);
    body.append('transaction',transaction);
    this.http.post(this.urlRoot+'etopup/inHistory',body,{responseType: 'text'}).subscribe(result => {
      console.log(result);
    });  
  }
  
  getRefilBonita(offset){
    let token = <any>{};
    this.getToken().then(token => {
      this.token = token;
      
      let url = this.service.url;
      let url2 = this.service.url2;
      
      let body: FormData = new FormData();
      body.append('token',this.token.access_token);
      body.append('offset',offset);
      
      console.log(offset);
      let data = this.http.post(url2+'getInternetDenom',body);
      data.subscribe(result => {
        this.items = result;
        // let arr:any = [];
        // arr = result; 
        // for (var i = 0; i < arr.length; ++i) {
        //   this.items.push(result[i]);
        // }
      });
      
    });
  }
  
  getEtopup(lim,page){
    let data:Observable<any>;
    data = this.http.get('http://150.242.111.235/apibonita/index.php/etopup/getEtopupDenomMobile/'+lim+'/'+page);
    data.subscribe(result => {
      for (var i = 0; i < result.data.length; ++i) {
        this.items.push(result.data[i]);
      }
    })
  }
  
  getChannelTopup(){
    let data = new FormData;
    data.append('id_user', localStorage.getItem('id'));
    this.http.post(this.service.urlRoot+'etopup/getChannelTopup',data).subscribe((result) => {
      this.balance = result['ballance'];
    });
  }
  
  getEtopupFind(find=''){
    this.getToken();
    let url = this.service.url;
    let url2 = this.service.url2;
    
    let body: FormData = new FormData();
    body.append('token',this.token);
    let data = this.http.post(url2+'getRefilBonita',body);
    data.subscribe(result => {
      this.items = result;
    });  
  }
  
  onInput(){
    this.items = [];
    this.lim = 0;
    this.perPage = 3;
    if (this.myInput == "") {
      this.getEtopup(this.lim,this.perPage);
      this.info = 1;
    }else{
      this.getEtopupFind(this.myInput); 
    }
  }
  
  doInfinite() {
    // for(let i = 0; i < 6;i++){
    //   this.offset = this.offset + 5;
    //   this.getRefilBonita(this.offset);  
    // }
  }
  
  toAlert(title,subtitle) {
    let alert = this.alertCtrl.create({
      title: title,
      subTitle: subtitle,
      buttons: ['Close']
    });
    alert.present();
  }
  
  inPaketData() {
    let res = this.f.value.data_package;
    res = res.split(',');
    let alert = this.alertCtrl.create({
      title: 'Konfirmasi Pembelian',
      message: 'Apakah anda yakin ingin menambahkan paket data '+res[1]+' '+res[3]+', dengan harga $'+res[2]+' dengan nomor tujuan ('+this.f.value.numeru+') ?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Yes',
          handler: () => {
            this.prosesPaketData(res[0],res[1],res[2],res[3],res[4]);
            console.log('Buy clicked');
          }
        }
      ]
    });
    alert.present();
  }
  
  logout() {
    const confirm = this.alertCtrl.create({
      title: "Logout",
      message: "Would you like to logout?",
      buttons: [
        {
          text: "No",
          handler: () => {
            console.log("Failed");
          }
        },
        {
          text: "Yes",
          handler: () => {
            localStorage.removeItem('id');
            const root = this.app.getRootNav();
            root.popToRoot();
          }
        }
      ]
    });
    confirm.present();
  }
  
  
}
