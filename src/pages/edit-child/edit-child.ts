import { Component } from '@angular/core';
import {IonicPage, NavController, NavParams,AlertController,LoadingController } from 'ionic-angular';
import { HttpClient } from "@angular/common/http";
import {Validators, FormBuilder, FormGroup,FormControl } from '@angular/forms';
// import { Geolocation } from '@ionic-native/geolocation';
import { SingletonService } from "../services/service";


/**
 * Generated class for the EditChildPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-edit-child',
  templateUrl: 'edit-child.html',
})
export class EditChildPage {

  private f : FormGroup;
  data:any;
  id:any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    // private geolocation: Geolocation,
    public service : SingletonService,
    private http: HttpClient,
    public loadingCtrl: LoadingController,
    private alertCtrl: AlertController,
    private formBuilder: FormBuilder
    ) {
    this.formBuilders();
    this.getChildID(this.navParams.data.id);
  }

  ionViewDidLoad() {
  }


  formBuilders(){
    this.f = this.formBuilder.group({
      name: ['', Validators.required],
      agent_number: ['', Validators.required],
      contact_number: [''],
      email : ['']
      // email: new FormControl('', Validators.compose([
      //   Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
      // ]))
    });
  }

  getChildID(id){
    this.http.get(this.service.urlRoot+'etopup/getChildID?id='+id).subscribe(value => {
      this.data = value;
      this.id = value[0].id;
      this.f.patchValue({name : value[0].channel_name});
      this.f.patchValue({contact_number : value[0].contact_number});
      this.f.patchValue({agent_number : value[0].master_number});
      this.f.patchValue({email : value[0].email});
    });
  }

  upChild(){
    let v = this.f.value;
    let data = new FormData();
    
    data.append('id', this.id);
    data.append('name', v.name);
    data.append('contact_number', v.contact_number);
    data.append('master_number', v.agent_number);
    data.append('email', v.email);

    this.http.post(this.service.urlRoot+'etopup/upChild',data).subscribe(d => {
      let data:any = d;
      if(data.status == false){
        this.toAlert('Failed','Agent Number cannot the same');
      }else if(data.status == true){
        this.toAlert('Success','Success edit data');
      }
    });
  }

  toAlert(title,subtitle) {
    let alert = this.alertCtrl.create({
      title: title,
      subTitle: subtitle,
      buttons: ['Close']
    });
    alert.present();
  }

}
