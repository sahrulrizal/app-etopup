import { Component } from '@angular/core';
import { Geolocation } from '@ionic-native/geolocation';
import { NativeGeocoder, NativeGeocoderReverseResult,NativeGeocoderOptions } from '@ionic-native/native-geocoder';
import {App, NavController, NavParams,AlertController,LoadingController } from 'ionic-angular';
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs/Observable";
import { SingletonService } from "../services/service";
import {Validators, FormBuilder, FormGroup } from '@angular/forms';

/**
* Generated class for the IsiPulsaPage page.
*
* See https://ionicframework.com/docs/components/#navigation for more info on
* Ionic pages and navigation.
*/

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  private f : FormGroup;
  items = [];
  lim = 0;
  perPage = 3;
  myInput;
  ldata : any = [];
  balance:any;
  location:any;
  
  info:any; /** untuk alert pencarian. */
  token:any;
  urlRoot:any = this.service.urlRoot;
  
  
  constructor(private geolocation: Geolocation,private nativeGeocoder: NativeGeocoder,public app: App,public loadingCtrl: LoadingController,private alertCtrl: AlertController,private formBuilder: FormBuilder,public navCtrl: NavController, public navParams: NavParams,private http: HttpClient,public service : SingletonService) {
  
  }
  getLocation(){
    let options: NativeGeocoderOptions = {
      useLocale: true,
      maxResults: 5
    };

    this.geolocation.getCurrentPosition().then((resp) => {
      this.nativeGeocoder.reverseGeocode(-8.5537992,125.5393888, options)
      .then((result: NativeGeocoderReverseResult[]) => {
        let info =  JSON.stringify(result[0].administrativeArea);
        this.location = info;
        // this.toAlert('Berhasil',info);
      })
      .catch((error: any) => console.log(error));
      
    }).catch((error) => {
    });

  }

  ionViewWillEnter(){
    this.getChannelTopup();
    console.log(this.balance);
    this.getLocation();
  }
  
 getChannelTopup(){
    let data = new FormData;
    data.append('id_user', localStorage.getItem('id'));
    this.http.post(this.service.urlRoot+'etopup/getChannelTopup',data).subscribe((result) => {
      this.balance = result['ballance'];
    });
  }
  
  ionViewDidLoad() {
    this.formBuilders();
    this.getEtopup(this.lim,this.perPage);
    this.langs(JSON.parse(localStorage.getItem("lang")));
    this.getToken();
  }
  
  formBuilders(){
    this.f = this.formBuilder.group({
      numeru: ['', Validators.required],
      pulsa: ['', Validators.required],
    });
  }
  
  
  
  getToken(){
    
    let i:Observable<any>;
    let url = this.service.url;
    i = this.http.get(url+'token');
    i.subscribe((data) => {
      this.token = data.access_token;
    });
  }
  
  rend(min,max){
    return min + Math.floor(Math.random() * (max - min + 1));
  }
  
  prosesTopup(ided,denom,amount,validity){
    const loader = this.loadingCtrl.create({
      content: 'Please Wait...',
    });
    loader.present();
    if(JSON.parse(amount) > JSON.parse(this.balance)){
      this.toAlert('Failed',"Can't continue to proccess E-topup, please check your balance!");
      loader.dismiss();
    }else{
      this.getToken();
      let url = this.service.url;
      let url2 = this.service.url2;
      let urlRoot = this.service.urlRoot;
      let addTopup = new FormData;
      
      if(this.location == undefined){
        this.location = JSON.stringify('');
      }

      addTopup.append('msisdn',this.f.value.numeru);
      addTopup.append('amount',amount);
      addTopup.append('location',JSON.parse(this.location));
      addTopup.append('ided',ided);
      addTopup.append('iduser',localStorage.getItem('id'));
      addTopup.append('channel',localStorage.getItem('idCh'));
      
      let data = this.http.post(urlRoot+'/etopup/addTopUpMobile',addTopup,{responseType: 'json'});
      data.subscribe(result => {
        if (result['success'] == true) {
        
        let lang;
        if (JSON.parse(localStorage.getItem("lang")) == 1) {
           lang = 'TL';
        }else if(JSON.parse(localStorage.getItem("lang")) == 2){
          lang = 'EN';
        }else if(JSON.parse(localStorage.getItem("lang")) == 3){
          lang = 'ID';
        }

          addTopup.append('lang',lang);

          addTopup.append('token',this.token);
          addTopup.append('info', '1');
          addTopup.append('isMobile', '1');
          addTopup.append('validity',validity);
          addTopup.append('trxid',this.rend(10,100000000000));
          addTopup.append('transaction',"Topup : "+denom);
          
          this.http.post(urlRoot+'etopup/isiTopup',addTopup,{responseType: 'text'}).subscribe(result2 => {
            let res = [];
            res = result2.split(' ');

            let msg = "";
             for(var i = 3; i < res.length ; i++){
              msg += res[i] + " ";
             }

            addTopup.append('m', msg);
            addTopup.append('trxCode', res[1]);

            if (res[2] == 0) {
              this.http.post(urlRoot+'etopup/sendSmsTopUp',addTopup,{responseType: 'text'}).subscribe(result3 => {console.log(result3+'tester')});
              this.http.get(urlRoot+'etopup/updateStatusTrx?id='+result['etopup_id']+'&idu='+localStorage.getItem('id')+'&amount='+amount+'&msisdn='+this.f.value.numeru+'&via=MOBILE_PULSA_DIGITAL').subscribe(result4 => {console.log(result4); });
              let msg = 'Top Up USD '+amount+' to '+this.f.value.numeru+' is success';
              // let msg = 'Sukses menambahkan pulsa '+denom+' USD ke nomor '+this.f.value.numeru+' , dengan harga $'+amount;
              // this.inHistory(this.f.value.numeru,msg,amount,"Topup Pulsa "+denom+' USD');
              this.toAlert('Success',msg);
              loader.dismiss();
              this.f.reset();
            }else{
              this.toAlert('Failed',"Can't continue to proccess E-topup, please check your number!");
              loader.dismiss();
            }
          });  
        }else{
          this.toAlert('Failed',"Error");
          loader.dismiss();
        }
      });
    }
  }
  
  inHistory(dest,msg,amount,transaction){
    let body = new FormData;
    body.append('idu',localStorage.getItem('id'));
    body.append('destination',dest);
    body.append('amount',amount);
    body.append('info','1');
    body.append('transaction',transaction);
    body.append('msg',msg);
    this.http.post(this.service.urlRoot+'etopup/inHistory',body,{responseType: 'text'}).subscribe(result => {
      console.log(result);
    });  
  }
  
  getEtopup(lim,page){
    let data:Observable<any>;
    data = this.http.get(this.service.urlRoot+'etopup/getEtopupDenomMobile/');
    data.subscribe(result => {
      for (var i = 0; i < result.data.length; ++i) {
        this.items.push({
          'id' : result.data[i].id,
          'denomination' : this.genCent(result.data[i].denomination),
          'amount' : result.data[i].amount,
          'validity' : result.data[i].validity,
          'nominal' : result.data[i].nominal,
          'money' : this.genMoney(result.data[i].amount)
        });
      }
    })
  }
  
  genCent(d){
    let n = d.split('.');
    if(n.length > 1){
      return n[1];
    }else{
      return n[0];
    }
  }
  
  genMoney(d){
    let n = d.split('.');
    if(n.length > 1){
      return 'Cent';
    }else{
      return 'USD';
    }
  }
  
  getEtopupFind(lim,page,find=''){
    let data:Observable<any>;
    if (find) {      
      data = this.http.get(this.service.urlRoot+'etopup/getEtopupDenomMobile/'+lim+'/'+page+'/'+find);
      data.subscribe(result => {
        for (var i = 0; i < result.data.length; ++i) {
          this.items.push(result.data[i]);
        }
        if(result.status == 0){
          this.info = 0;
        }else{
          this.info = 1;
        }
        
      })
    }
  }
  
  onInput(){
    this.items = [];
    this.lim = 0;
    this.perPage = 3;
    if (this.myInput == "") {
      this.getEtopup(this.lim,this.perPage);
      this.info = 1;
    }else{
      this.getEtopupFind(this.lim,this.perPage,this.myInput); 
    }
  }
  
  doInfinite(infiniteScroll) {
    
    setTimeout(() => {
      this.lim  = this.lim + this.perPage;
      this.getEtopup(this.lim,this.perPage); 
      console.log('Async operation has ended');
      infiniteScroll.complete();
    }, 500);
  }
  
  toAlert(title,subtitle) {
    let alert = this.alertCtrl.create({
      title: title,
      subTitle: subtitle,
      buttons: ['Close']
    });
    alert.present();
  }
  
  inTopup() {
    let res = this.f.value.pulsa;
    res = res.split(',');
    let alert = this.alertCtrl.create({
      title: 'Konfirmasi Pembelian',
      message: 'Apakah anda yakin ingin menambahkan pulsa '+res[1]+' USD, dengan harga $'+res[2]+' dengan nomor tujuan ('+this.f.value.numeru+') ?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Yes',
          handler: () => {
            this.prosesTopup(res[0],res[1],res[2],res[3]);
            console.log('Buy clicked');
          }
        }
      ]
    });
    alert.present();
  }
  
  langs(lang){
    let data:Observable<any>;
    data = this.http.get('assets/data/lang.json');
    data.subscribe(result => {
      for (var i = 0; i < result['lang'].length; ++i) {
        if (result['lang'][i].lang == lang) {
          this.ldata.push(result['lang'][i]);
        }
      }
    })
  }
  
  logout() {
    const confirm = this.alertCtrl.create({
      title: "Logout",
      message: "Would you like to logout?",
      buttons: [
        {
          text: "No",
          handler: () => {
            console.log("Failed");
          }
        },
        {
          text: "Yes",
          handler: () => {
            localStorage.removeItem('id');
            const root = this.app.getRootNav();
            root.popToRoot();
          }
        }
      ]
    });
    confirm.present();
  }
  
}
