import { Component } from '@angular/core';
import { Platform,App } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { TabsPage } from '../pages/tabs/tabs';
import { LoginPage } from '../pages/login/login';


@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any = LoginPage;
  no:any = 1;
  constructor(platform: Platform,public app : App, statusBar: StatusBar, splashScreen: SplashScreen) {
    
    platform.ready().then((d) => {
      platform.pause
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      // splashScreen.hide();
    });

    let t = 3600; 
     platform.ready().then(() => { 
       let time = t;  
        platform.pause.subscribe((d) => {
          setInterval( ()=>{
            time = time-1;
          });
        });

        platform.resume.subscribe(() => {
          if(time <= 0){
            localStorage.removeItem('id');
            const root = this.app.getRootNav();
            root.popToRoot();
          }else{
            time = t;
          }
        });
    });

  }

}
