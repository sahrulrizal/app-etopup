import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { AboutPage } from '../pages/about/about';
import { ContactPage } from '../pages/contact/contact';
import { HomePage } from '../pages/home/home';
import { AddChildPage } from '../pages/add-child/add-child';
import { TabsPage } from '../pages/tabs/tabs';
import { TransferPage } from '../pages/transfer/transfer';
import { HistoryPage } from '../pages/history/history';
import { PaketDataPage } from '../pages/paket-data/paket-data';
import { InvoicesPage } from '../pages/invoices/invoices';

import { LoginPageModule } from '../pages/login/login.module';
import { IsiPulsaPageModule } from '../pages/isi-pulsa/isi-pulsa.module';

import { HTTP } from '@ionic-native/http';
import { HttpClientModule } from "@angular/common/http";
import { HttpModule } from '@angular/http'; 

import { SingletonService } from '../pages/services/service';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Calendar } from '@ionic-native/calendar';
import { ViewChildPageModule } from '../pages/view-child/view-child.module';
import { Geolocation } from '@ionic-native/geolocation';
import { NewPasswordPage } from '../pages/new-password/new-password';
import { NativeGeocoder } from '@ionic-native/native-geocoder';
import { Diagnostic } from '@ionic-native/diagnostic';
import { OpenNativeSettings } from '@ionic-native/open-native-settings';
import { EditChildPageModule } from '../pages/edit-child/edit-child.module';

@NgModule({
  declarations: [
    MyApp,
    AboutPage,
    ContactPage,
    NewPasswordPage,
    HomePage,
    HistoryPage,
    PaketDataPage,
    TransferPage,
    TabsPage,
    InvoicesPage,
    AddChildPage
  ],
  imports: [
    HttpModule,
    BrowserModule,
    IsiPulsaPageModule,
    ViewChildPageModule,
    EditChildPageModule,
    LoginPageModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AboutPage,
    AddChildPage,
    ContactPage,
    HomePage,
    HistoryPage,
    PaketDataPage,
    TransferPage,
    TabsPage,
    InvoicesPage,
    NewPasswordPage
  ],
  providers: [
    HTTP,
    SingletonService,
    StatusBar,
    Calendar,
    Geolocation,
    Diagnostic,
    NativeGeocoder,
    SplashScreen,
    OpenNativeSettings,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
